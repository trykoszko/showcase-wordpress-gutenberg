<?php
/**
 * Class ClassMtFormTest
 *
 * @package Mt_Form
 */

require_once dirname( __DIR__ ) . '/inc/class-mt-form.php';

use Brain\Monkey;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use PHPUnit\Framework\TestCase;

final class ClassMtFormTest extends TestCase {

	use MockeryPHPUnitIntegration;

	public function setUp(): void {
		parent::setUp();

		Monkey\setUp();

		Brain\Monkey\Functions\when( '__' )->returnArg();
		Brain\Monkey\Functions\when( 'sanitize_text_field' )->returnArg();
		Brain\Monkey\Functions\when( 'sanitize_email' )->returnArg();
		Brain\Monkey\Functions\when( 'sanitize_textarea_field' )->returnArg();
	}

	public function tearDown(): void {
		Brain\Monkey\tearDown();

		parent::tearDown();
	}

	public function testInit() {
		$mtForm = new Mt_Form();

		Brain\Monkey\Actions\expectAdded( 'init' )
			->once()
			->with( array( $mtForm, 'loadPluginTextdomain' ), 10, 0 );

		Brain\Monkey\Actions\expectAdded( 'init' )
			->once()
			->with( array( $mtForm, 'enqueueScripts' ), 10, 0 );

		Brain\Monkey\Actions\expectAdded( 'wp_ajax_store_mt_form_submission' )
			->once()
			->with( array( $mtForm, 'storeFormSubmission' ), 10, 0 );

		Brain\Monkey\Actions\expectAdded( 'wp_ajax_nopriv_store_mt_form_submission' )
			->once()
			->with( array( $mtForm, 'storeFormSubmission' ), 10, 0 );

		Brain\Monkey\Actions\expectAdded( 'wp_ajax_get_mt_form_submissions' )
			->once()
			->with( array( $mtForm, 'getPaginatedFormSubmissions' ), 10, 0 );

		Brain\Monkey\Actions\expectAdded( 'wp_ajax_nopriv_get_mt_form_submissions' )
			->once()
			->with( array( $mtForm, 'getPaginatedFormSubmissions' ), 10, 0 );

		Brain\Monkey\Filters\expectAdded( 'the_content' )
			->once()
			->with( array( $mtForm, 'restrictAccessToPagesWithSubmissionsBlock' ) );

		$mtForm->init();
	}

	public function testLoadPluginTextdomain() {
		$mtForm = new Mt_Form();

		Brain\Monkey\Functions\expect( 'load_plugin_textdomain' )
			->once()
			->with( 'mt-form', false, dirname( __DIR__ ) . '/languages' );

		$mtForm->loadPluginTextdomain();
	}

	public function testEnqueueScripts() {
		$blockNames = Mt_Form::$blockNames;
		$blockCount = count( $blockNames );

		$userMock = (object) array(
			'first_name' => 'John',
			'last_name'  => 'Doe',
			'user_email' => 'john.doe@example.com',
		);

		Monkey\Functions\when( 'wp_get_current_user' )->justReturn( $userMock );
		Monkey\Functions\when( 'plugins_url' )->justReturn( 'http://example.com/script.js' );
		Monkey\Functions\when( 'admin_url' )->justReturn( 'http://example.com/admin' );
		Monkey\Functions\when( 'wp_create_nonce' )->justReturn( 'nonce_value' );

		Monkey\Functions\expect( 'wp_register_script' )->times( $blockCount * 2 );
		Monkey\Functions\expect( 'wp_localize_script' )->times( $blockCount * 2 );
		Monkey\Functions\expect( 'wp_enqueue_script' )->times( $blockCount );
		Monkey\Functions\expect( 'wp_enqueue_style' )->times( $blockCount * 2 );
		Monkey\Functions\expect( 'register_block_type' )->times( $blockCount );

		$mtForm = new Mt_Form();
		$mtForm->enqueueScripts();
	}

	public function testStoreFormSubmissionSuccess() {
		global $wpdb;

		$wpdb         = Mockery::mock( 'WPDB' );
		$wpdb->prefix = 'wp_';
		$wpdb->shouldReceive( 'insert' )->once()->andReturn( true );

		Brain\Monkey\Functions\when( 'wp_verify_nonce' )->justReturn( true );

		Brain\Monkey\Functions\expect( 'wp_send_json' )
			->once()
			->with( Mockery::on( function ( $arg ) {
				return $arg['message'] === 'Form submission successful.';
			} ) );

		$mtForm = new Mt_Form();

		$_REQUEST = array(
			'nonceKey'  => 'nonceVal',
			'firstName' => 'John',
			'lastName'  => 'Doe',
			'email'     => 'john@doe.com',
			'subject'   => 'Test subject',
			'message'   => 'Test message',
		);

		$mtForm->storeFormSubmission();

		$_REQUEST = array();
	}

	public function testStoreFormSubmissionNonceFailed() {
		global $wpdb;

		$wpdb         = Mockery::mock( 'WPDB' );
		$wpdb->prefix = 'wp_';
		$wpdb->shouldReceive( 'insert' )->once()->andReturn( true );

		Brain\Monkey\Functions\when( 'wp_verify_nonce' )->justReturn( false );

		Brain\Monkey\Functions\expect( 'wp_send_json' )
			->once()
			->with( Mockery::on( function ( $arg ) {
				return $arg['message'] === 'Nonce verification failed.';
			} ),    401 );

		$mtForm = new Mt_Form();

		$_REQUEST = array(
			'nonceKey'  => 'nonceVal',
			'firstName' => 'John',
			'lastName'  => 'Doe',
			'email'     => 'john@doe.com',
			'subject'   => 'Test subject',
			'message'   => 'Test message',
		);

		$mtForm->storeFormSubmission();

		$_REQUEST = array();
	}

	public function testStoreFormSubmissionParametersMissing() {
		global $wpdb;

		$wpdb         = Mockery::mock( 'WPDB' );
		$wpdb->prefix = 'wp_';
		$wpdb->shouldReceive( 'insert' )->once()->andReturn( true );

		Brain\Monkey\Functions\when( 'wp_verify_nonce' )->justReturn( true );

		Brain\Monkey\Functions\expect( 'wp_send_json' )
			->once()
			->with( Mockery::on( function ( $arg ) {
				return $arg['message'] === 'All fields are required.';
			} ),    400 );

		$mtForm = new Mt_Form();

		$_REQUEST = array(
			'nonceKey'  => 'nonceVal',
			'firstName' => null,
			'lastName'  => null,
			'email'     => 'john@doe.com',
			'subject'   => 'Test subject',
			'message'   => 'Test message',
		);

		$mtForm->storeFormSubmission();

		$_REQUEST = array();
	}

	public function testGetPaginatedFormSubmissions() {
		global $wpdb;

		$wpdb         = Mockery::mock( 'WPDB' );
		$wpdb->prefix = 'wp_';
		$tableName    = $wpdb->prefix . MT_FORM_DB_TABLE_NAME;

		Brain\Monkey\Functions\when( 'wp_verify_nonce' )->justReturn( true );
		Brain\Monkey\Functions\when( 'current_user_can' )->justReturn( true );

		$wpdb->shouldReceive( 'prepare' )
		     ->once()
		     ->with(
			     Mockery::type( 'string' ),
			     Mockery::type( 'int' ),
			     Mockery::type( 'int' )
		     )
		     ->andReturn( "SELECT * FROM $tableName ORDER BY id DESC LIMIT 10 OFFSET 0" );

		$wpdb->shouldReceive( 'get_results' )
		     ->once()
		     ->with( "SELECT * FROM $tableName ORDER BY id DESC LIMIT 10 OFFSET 0" )
		     ->andReturn( Mockery::type( 'array' ) );

		$wpdb->shouldReceive( 'get_var' )
		     ->once()
		     ->with( "SELECT COUNT(*) FROM $tableName" )
		     ->andReturn( 10 );

		Brain\Monkey\Functions\expect( 'wp_send_json' )
			->once()
			->with( Mockery::on( function ( $arg ) {
				return is_array( $arg )
				       && array_key_exists( 'data', $arg )
				       && array_key_exists( 'meta', $arg )
				       && is_array( $arg['meta'] )
				       && array_key_exists( 'page', $arg['meta'] )
				       && array_key_exists( 'limit', $arg['meta'] )
				       && array_key_exists( 'total', $arg['meta'] );
			} ) );

		$mtForm = new Mt_Form();

		$mtForm->getPaginatedFormSubmissions();

		$_REQUEST = array();
	}

	public function testGetPaginatedFormSubmissionsNonceFailed() {
		global $wpdb;

		$wpdb         = Mockery::mock( 'WPDB' );
		$wpdb->prefix = 'wp_';
		$tableName    = $wpdb->prefix . MT_FORM_DB_TABLE_NAME;

		Brain\Monkey\Functions\when( 'wp_verify_nonce' )->justReturn( false );
		Brain\Monkey\Functions\when( 'current_user_can' )->justReturn( true );

		$wpdb->shouldReceive( 'prepare' )
		     ->once()
		     ->with(
			     Mockery::type( 'string' ),
			     Mockery::type( 'int' ),
			     Mockery::type( 'int' )
		     )
		     ->andReturn( "SELECT * FROM $tableName ORDER BY id DESC LIMIT 10 OFFSET 0" );

		$wpdb->shouldReceive( 'get_results' )
		     ->once()
		     ->with( "SELECT * FROM $tableName ORDER BY id DESC LIMIT 10 OFFSET 0" )
		     ->andReturn( Mockery::type( 'array' ) );

		$wpdb->shouldReceive( 'get_var' )
		     ->once()
		     ->with( "SELECT COUNT(*) FROM $tableName" )
		     ->andReturn( 10 );

		Brain\Monkey\Functions\expect( 'wp_send_json' )
			->once()
			->with( Mockery::on( function ( $arg ) {
				return $arg['message'] === 'Nonce verification failed.';
			} ),    401 );

		$mtForm = new Mt_Form();

		$mtForm->getPaginatedFormSubmissions();

		$_REQUEST = array();
	}

	public function testGetPaginatedFormSubmissionsRejectNonAdmins() {
		global $wpdb;

		$wpdb         = Mockery::mock( 'WPDB' );
		$wpdb->prefix = 'wp_';
		$tableName    = $wpdb->prefix . MT_FORM_DB_TABLE_NAME;

		Brain\Monkey\Functions\when( 'wp_verify_nonce' )->justReturn( true );
		Brain\Monkey\Functions\when( 'current_user_can' )->justReturn( false );

		$wpdb->shouldReceive( 'prepare' )
		     ->once()
		     ->with(
			     Mockery::type( 'string' ),
			     Mockery::type( 'int' ),
			     Mockery::type( 'int' )
		     )
		     ->andReturn( "SELECT * FROM $tableName ORDER BY id DESC LIMIT 10 OFFSET 0" );

		$wpdb->shouldReceive( 'get_results' )
		     ->once()
		     ->with( "SELECT * FROM $tableName ORDER BY id DESC LIMIT 10 OFFSET 0" )
		     ->andReturn( Mockery::type( 'array' ) );

		$wpdb->shouldReceive( 'get_var' )
		     ->once()
		     ->with( "SELECT COUNT(*) FROM $tableName" )
		     ->andReturn( 10 );

		Brain\Monkey\Functions\expect( 'wp_send_json' )
			->once()
			->with( Mockery::on( function ( $arg ) {
				return $arg['message'] === 'Insufficient permissions.';
			} ),    403 );

		$mtForm = new Mt_Form();

		$mtForm->getPaginatedFormSubmissions();

		$_REQUEST = array();
	}

	public function testRestrictAccessToPagesWithSubmissionsBlock() {
		$content = '<!-- wp:mt-form/mt-form -->
<div class="wp-block-mt-form-mt-form"><div class="mt-form-block-element"><h3 class="mt-form-block-element__title">Submit your feedback</h3><form class="mt-form-block-element__form"><label class="mt-form-block-element__item"><p class="mt-form-block-element__item__label">First name<span class="mt-form-block-element__item__label__asterisk">*</span></p><input class="mt-form-block-element__item__input" type="text" name="firstName" required minlength="2"/></label><label class="mt-form-block-element__item"><p class="mt-form-block-element__item__label">Last name<span class="mt-form-block-element__item__label__asterisk">*</span></p><input class="mt-form-block-element__item__input" type="text" name="lastName" required minlength="2"/></label><label class="mt-form-block-element__item mt-form-block-element__item--wide"><p class="mt-form-block-element__item__label">Email<span class="mt-form-block-element__item__label__asterisk">*</span></p><input class="mt-form-block-element__item__input" type="email" name="email" required/></label><label class="mt-form-block-element__item mt-form-block-element__item--wide"><p class="mt-form-block-element__item__label">Subject<span class="mt-form-block-element__item__label__asterisk">*</span></p><input class="mt-form-block-element__item__input" type="text" name="subject" required minlength="5"/></label><label class="mt-form-block-element__item mt-form-block-element__item--wide"><p class="mt-form-block-element__item__label">Message<span class="mt-form-block-element__item__label__asterisk">*</span></p><textarea rows="8" class="mt-form-block-element__item__input" name="message" minlength="30"></textarea></label><div class="mt-form-block-element__footer"><button class="mt-form-block-element__button" name="submit">Submit</button></div></form></div></div>
<!-- /wp:mt-form/mt-form -->

<!-- wp:mt-form/mt-form-entries-table -->
<div class="wp-block-mt-form-mt-form-entries-table"><div class="wp-block-mt-form-mt-form-entries-table-block"><h3 class="wp-block-mt-form-mt-form-entries-table__title">Form submissions</h3><div class="wp-block-mt-form-mt-form-entries-table__fetch-button"><button>Fetch entries</button></div></div></div>
<!-- /wp:mt-form/mt-form-entries-table -->';

		$blocks = [
			[
				'blockName' => 'mt-form/mt-form',
			],
			[
				'blockName' => 'mt-form/mt-form-entries-table',
			],
		];

		$mtForm = new Mt_Form();

		Brain\Monkey\Functions\when( 'is_singular' )->justReturn( true );
		Brain\Monkey\Functions\when( 'current_user_can' )->justReturn( false );
		Brain\Monkey\Functions\when( 'get_the_content' )->justReturn( $content );
		Brain\Monkey\Functions\when( 'parse_blocks' )->justReturn( $blocks );

		$filterReturn = $mtForm->restrictAccessToPagesWithSubmissionsBlock( $content );

		$this->assertNotFalse( $filterReturn === 'Sorry, this content is only available for administrators.' );
	}

}
