<?php
/**
 * PHPUnit bootstrap file.
 *
 * @package Mt_Form
 */

require_once dirname( __DIR__ ) . '/vendor/autoload.php';
require_once dirname( __DIR__ ) . '/constants.php';
