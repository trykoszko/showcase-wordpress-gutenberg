<?php
/**
 * Class Mt_Form
 *
 * @package Mt_Form
 * Submission collection and display block.
 *
 * @since 0.1.0
 * @version 0.1.0
 */

class Mt_Form {

	/**
	 * @var string[] $blockNames Block names and slugs.
	 */
	public static $blockNames = [
		"form"          => "mt-form",
		"entries-table" => "mt-form-entries-table",
	];

	/**
	 * Constructor.
	 */
	public function __construct() {
	}

	/**
	 * Run the plugin.
	 *
	 * @return void
	 */
	public function init() {
		add_action( 'init', array( $this, 'loadPluginTextdomain' ), 10, 0 );
		add_action( 'init', array( $this, 'enqueueScripts' ), 10, 0 );

		add_action( 'wp_ajax_store_mt_form_submission', array( $this, 'storeFormSubmission' ), 10, 0 );
		add_action( 'wp_ajax_nopriv_store_mt_form_submission', array( $this, 'storeFormSubmission' ), 10, 0 );

		add_action( 'wp_ajax_get_mt_form_submissions', array( $this, 'getPaginatedFormSubmissions' ), 10, 0 );
		add_action( 'wp_ajax_nopriv_get_mt_form_submissions', array( $this, 'getPaginatedFormSubmissions' ), 10, 0 );

		add_filter( 'the_content', array( $this, 'restrictAccessToPagesWithSubmissionsBlock' ), 10, 1 );
	}

	/**
	 * Load plugin textdomain.
	 *
	 * @return void
	 */
	public function loadPluginTextdomain() {
		load_plugin_textdomain( 'mt-form', false, dirname( __DIR__ ) . '/languages' );
	}

	/**
	 * Enqueue, localize and register Block scripts.
	 * Could be registered via block.json but this is more flexible and allows for localization in edit.js
	 *
	 * @return void
	 */
	public function enqueueScripts() {
		$userData   = wp_get_current_user();
		$assetTypes = [ 'index', 'view' ];

		foreach ( self::$blockNames as $blockName => $blockSlug ) {
			foreach ( $assetTypes as $assetType ) {
				$scriptName = "$blockSlug-$assetType-script";
				$assetFile  = include( dirname( __DIR__ ) . "/blocks/$blockName/build/$assetType.asset.php" );

				wp_register_script(
					$scriptName,
					plugins_url( "/blocks/$blockName/build/$assetType.js", __DIR__ ),
					$assetFile["dependencies"],
					$assetFile["version"]
				);

				wp_localize_script(
					$scriptName,
					"blockData",
					array(
						"ajaxUrl"  => admin_url( "admin-ajax.php" ),
						"nonce"    => wp_create_nonce( "mt_form_nonce" ),
						"userData" => $userData ? array(
							"firstName" => $userData->first_name ?? "",
							"lastName"  => $userData->last_name ?? "",
							"email"     => $userData->user_email ?? "",
						) : null,
					)
				);

				if ( $assetType === 'index' ) {
					wp_enqueue_style(
						"$blockSlug-$assetType-style",
						plugins_url( "/blocks/$blockName/build/style-$assetType.css", __DIR__ )
					);

					wp_enqueue_style(
						"$blockSlug-$assetType-style-index",
						plugins_url( "/blocks/$blockName/build/$assetType.css", __DIR__ )
					);

					register_block_type(
						"mt-form/$blockSlug",
						array(
							'editor_script' => $scriptName,
							'style'         => "$blockSlug-$assetType-style",
						)
					);
				} else {
					wp_enqueue_script( $scriptName );
				}
			}
		}
	}

	/**
	 * Store form submission.
	 *
	 * @return void
	 */
	public function storeFormSubmission() {
		$submissionData = $_REQUEST;

		$nonce = isset( $submissionData['nonce'] ) ? $submissionData['nonce'] : '';

		if ( ! wp_verify_nonce( $nonce, 'mt_form_nonce' ) ) {
			wp_send_json( array(
				              'message' => __( 'Nonce verification failed.', 'mt-form' ),
			              ), 401 );
		}

		global $wpdb;

		$tableName = $wpdb->prefix . MT_FORM_DB_TABLE_NAME;

		if ( empty( $submissionData['firstName'] ) || empty( $submissionData['lastName'] ) || empty( $submissionData['email'] ) || empty( $submissionData['subject'] ) || empty( $submissionData['message'] ) ) {
			wp_send_json( array(
				              'message' => __( 'All fields are required.', 'mt-form' ),
			              ), 400 );
		}

		$sanitizedData = array(
			'first_name' => sanitize_text_field( $submissionData['firstName'] ),
			'last_name'  => sanitize_text_field( $submissionData['lastName'] ),
			'email'      => sanitize_email( $submissionData['email'] ),
			'subject'    => sanitize_text_field( $submissionData['subject'] ),
			'message'    => sanitize_textarea_field( $submissionData['message'] ),
		);

		$wpdb->insert(
			$tableName,
			$sanitizedData
		);

		wp_send_json( array(
			              'message' => __( 'Form submission successful.', 'mt-form' ),
		              ) );
	}

	/**
	 * Get paginated Form submissions.
	 *
	 * @return void
	 */
	public function getPaginatedFormSubmissions() {
		$submissionData = $_REQUEST;

		$nonce = isset( $submissionData['nonce'] ) ? $submissionData['nonce'] : '';

		if ( ! wp_verify_nonce( $nonce, 'mt_form_nonce' ) ) {
			wp_send_json( array(
				              'message' => __( 'Nonce verification failed.', 'mt-form' ),
			              ), 401 );
		}

		if ( ! current_user_can( 'administrator' ) ) {
			wp_send_json( array(
				              'message' => __( 'Insufficient permissions.', 'mt-form' ),
			              ), 403 );
		}

		global $wpdb;

		$tableName = $wpdb->prefix . MT_FORM_DB_TABLE_NAME;

		$page  = isset( $submissionData['page'] ) ? intval( $submissionData['page'] ) : 1;
		$limit = isset( $submissionData['limit'] ) ? intval( $submissionData['limit'] ) : 10;

		$offset = ( $page - 1 ) * $limit;

		$sql = $wpdb->prepare(
			"SELECT * FROM $tableName ORDER BY id DESC LIMIT %d OFFSET %d",
			$limit,
			$offset
		);

		wp_send_json( array(
			              'data' => $wpdb->get_results( $sql ),
			              'meta' => array(
				              'page'  => $page,
				              'limit' => $limit,
				              'total' => intval( $wpdb->get_var( "SELECT COUNT(*) FROM $tableName" ) ),
			              ),
		              ) );
	}

	/**
	 * Filter content to restrict access to pages with submissions block.
	 *
	 * @return void
	 */
	public function restrictAccessToPagesWithSubmissionsBlock( $content ) {
		if ( is_singular() && ! current_user_can( 'administrator' ) ) {
			$blockName   = 'mt-form/mt-form-entries-table';
			$postContent = get_the_content();
			$blocks      = parse_blocks( $postContent );

			foreach ( $blocks as $block ) {
				if ( $block['blockName'] === $blockName ) {
					return __( 'Sorry, this content is only available for administrators.', 'mt-form' );
				}
			}
		}

		return $content;
	}

}
