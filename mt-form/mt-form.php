<?php
/**
 * Plugin Name:       MT Form
 * Description:       Submission collection and display block.
 * Requires at least: 6.1
 * Requires PHP:      7.4
 * Version:           0.1.0
 * Author:            Michal Trykoszko
 * License:           GPL-2.0-or-later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       mt-form
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

require_once dirname( __FILE__ ) . '/constants.php';

/**
 * Plugin activation hook function
 */
function mt_form_activate() {
	global $wpdb;

	$tableName      = $wpdb->prefix . MT_FORM_DB_TABLE_NAME;
	$charsetCollate = $wpdb->get_charset_collate();

	$sql = "CREATE TABLE IF NOT EXISTS $tableName (
		id mediumint(9) NOT NULL AUTO_INCREMENT,
		first_name varchar(100) NOT NULL,
		last_name varchar(500) NOT NULL,
		email varchar(100) NOT NULL,
		subject varchar(100) NOT NULL,
		message text NOT NULL,
		created_at datetime DEFAULT now() NOT NULL,
		PRIMARY KEY  (id)
	) $charsetCollate;";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );
}

register_activation_hook( __FILE__, 'mt_form_activate' );

require_once dirname( __FILE__ ) . '/inc/class-mt-form.php';

$plugin = new Mt_Form();
$plugin->init();
