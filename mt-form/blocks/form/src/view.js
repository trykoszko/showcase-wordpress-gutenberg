import {render, Suspense} from '@wordpress/element';
import domReady from '@wordpress/dom-ready';
import {Loader} from "./components/Loader";
import {FormWrapper} from "./components/FormWrapper";

domReady(() => {
	const forms = document.querySelectorAll('.wp-block-mt-form-mt-form');
	if (forms?.length) {
		Array.from(forms).forEach(form => {
			render(
				<Suspense fallback={<Loader/>}>
					<FormWrapper data={blockData}/>
				</Suspense>,
				form
			)
		});
	}
});
