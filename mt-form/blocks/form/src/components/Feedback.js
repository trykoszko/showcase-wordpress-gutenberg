import React from 'react';
import {__} from "@wordpress/i18n";

export const Feedback = (props) => {
	const {error} = props;

	return (
		<p className="mt-form-block-feedback-element">
			{error ?? __('Thank you for sending us your feedback', 'mt-form')}
		</p>
	)
}
