import React, {useEffect, useState} from 'react';
import {Form} from "./Form";
import {Feedback} from "./Feedback";

export const FormWrapper = (props) => {
	const {isEditor = false, data = {}} = props;
	const [isLoading, setIsLoading]     = useState(false);
	const [isSent, setIsSent]           = useState(false);
	const [error, setError]             = useState(null);
	const [formData, setFormData]       = useState(data?.userData || {});

	useEffect(() => {
		if (data?.userData) {
			setFormData(data.userData);
		}
	}, [data?.userData])

	const handleSubmit = async (e) => {
		e.preventDefault();

		setError(null);
		setIsLoading(true);

		const requestUrlParams = new URLSearchParams()
		requestUrlParams.append('action', 'store_mt_form_submission')
		requestUrlParams.append('nonce', data.nonce)

		const requestBody = new FormData();
		for (const key in formData) {
			requestBody.append(key, formData[key]);
		}

		const response     = await fetch(`${data.ajaxUrl}?${requestUrlParams}`, {
			method: 'POST',
			body: requestBody
		});
		const responseBody = await response.json();

		if (response.ok) {
			setIsSent(true);
		} else {
			setError(responseBody?.message ?? __('An error occurred', 'mt-form'));
		}

		setIsLoading(false);
	}

	return (
		<div className={"wp-block-mt-form-mt-form"}>
			{isSent ? (
				<Feedback error={error}/>
			) : (
				<Form
					handleSubmit={handleSubmit}
					isEditor={isEditor}
					isLoading={isLoading}
					isSent={isSent}
					formData={formData}
					setFormData={setFormData}
				/>
			)}
		</div>
	)
}
