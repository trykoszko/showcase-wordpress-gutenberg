import React from 'react';
import {__} from "@wordpress/i18n";
import {Loader} from "./Loader";

export const Form = (props) => {
	const {
			  isLoading    = false,
			  isEditor     = false,
			  handleSubmit = false,
			  formData     = {},
			  setFormData  = false
		  } = props;

	const setValue = e => {
		setFormData({
			...formData,
			[e.target.name]: e.target.value
		});
	}

	return (
		<div className="wp-block-mt-form-mt-form__component">

			{isLoading && (
				<div className="mt-form-block-element__loading-overlay">
					<Loader/>
				</div>
			)}

			<h3 className={'mt-form-block-element__title'}>{__('Submit your feedback', 'mt-form')}</h3>

			{/* @TODO: any react form lib e.g. react-hook-form would be very useful here */}
			<form className={'mt-form-block-element__form'} onSubmit={handleSubmit}>
				<label className="mt-form-block-element__item">
					<p className="mt-form-block-element__item__label">
						{__('First name', 'mt-form')}
						<span className="mt-form-block-element__item__label__asterisk">*</span>
					</p>
					<input className="mt-form-block-element__item__input" type="text" name="firstName"
						   value={formData?.firstName} disabled={isLoading || isEditor}
						   onChange={setValue}
						   required
						   minLength={2}
					/>
				</label>

				<label className="mt-form-block-element__item">
					<p className="mt-form-block-element__item__label">
						{__('Last name', 'mt-form')}
						<span className="mt-form-block-element__item__label__asterisk">*</span>
					</p>
					<input className="mt-form-block-element__item__input" type="text" name="lastName"
						   value={formData?.lastName} disabled={isLoading || isEditor}
						   onChange={setValue}
						   required
						   minLength={2}
					/>
				</label>

				<label className="mt-form-block-element__item mt-form-block-element__item--wide">
					<p className="mt-form-block-element__item__label">
						{__('Email', 'mt-form')}
						<span className="mt-form-block-element__item__label__asterisk">*</span>
					</p>
					<input className="mt-form-block-element__item__input" type="email" name="email"
						   value={formData?.email} disabled={isLoading || isEditor}
						   onChange={setValue}
						   required
					/>
				</label>

				<label className="mt-form-block-element__item mt-form-block-element__item--wide">
					<p className="mt-form-block-element__item__label">
						{__('Subject', 'mt-form')}
						<span className="mt-form-block-element__item__label__asterisk">*</span>
					</p>
					<input className="mt-form-block-element__item__input" type="text" name="subject"
						   disabled={isLoading || isEditor}
						   onChange={setValue}
						   required
						   minLength={5}
					/>
				</label>

				<label className="mt-form-block-element__item mt-form-block-element__item--wide">
					<p className="mt-form-block-element__item__label">
						{__('Message', 'mt-form')}
						<span className="mt-form-block-element__item__label__asterisk">*</span>
					</p>
					<textarea rows={8} className="mt-form-block-element__item__input" name="message"
							  disabled={isLoading || isEditor}
							  onChange={setValue}
							  minLength={30}
					/>
				</label>

				<div className={"mt-form-block-element__footer"}>
					<button className={"mt-form-block-element__button"}
							name="submit"
							disabled={isLoading || isEditor}
					>{__('Submit', 'mt-form')}</button>
				</div>
			</form>
		</div>
	);
}
