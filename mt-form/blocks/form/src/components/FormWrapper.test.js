import React from 'react';
import {render} from '@testing-library/react'
import '@testing-library/jest-dom'
import {FormWrapper} from "./FormWrapper";

test('renders form-wrapper component with form component inside', () => {
	const {getByText, container} = render(<FormWrapper
		isEditor={false}
		data={{
			ajaxUrl: 'http://localhost/wp-admin/admin-ajax.php',
			nonce: 'test-nonce-123',
			userData: {
				firstName: 'John',
				lastName: 'Doe',
				email: 'john@doe.com'
			}
		}}
	/>)

	expect(container.querySelector('.wp-block-mt-form-mt-form')).toBeInTheDocument();
	expect(container.querySelector('form')).toBeInTheDocument();
})
