import React from 'react';
import {render} from '@testing-library/react'
import {Feedback} from './Feedback'
import '@testing-library/jest-dom'

test('renders feedback component', () => {
	const {getByText} = render(<Feedback/>)

	expect(getByText('Thank you for sending us your feedback')).toBeInTheDocument();
})

test('renders feedback component with error message', () => {
	const {getByText} = render(<Feedback error={'An error occurred'}/>)

	expect(getByText('An error occurred')).toBeInTheDocument();
})
