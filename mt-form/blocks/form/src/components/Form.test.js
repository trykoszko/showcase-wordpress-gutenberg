import React from 'react';
import {fireEvent, render, waitFor} from '@testing-library/react'
import '@testing-library/jest-dom'
import {Form} from "./Form";

test('renders form component', () => {
	const {getByText, container} = render(<Form
		isLoading={false}
		isEditor={false}
		handleSubmit={() => {
		}}
		formData={{}}
		setFormData={false}
	/>)

	expect(getByText('Submit your feedback')).toBeInTheDocument();
	expect(container.querySelector('input[name="firstName"]')).toBeInTheDocument();
	expect(container.querySelector('input[name="lastName"]')).toBeInTheDocument();
	expect(container.querySelector('input[name="email"]')).toBeInTheDocument();
	expect(container.querySelector('input[name="subject"]')).toBeInTheDocument();
	expect(container.querySelector('textarea[name="message"]')).toBeInTheDocument();
	expect(container.querySelector('button[name="submit"]')).toBeInTheDocument();
})

test('renders form component with loading overlay', () => {
	const {getByText, container} = render(<Form
		isLoading={true}
		isEditor={false}
		handleSubmit={() => {
		}}
		formData={{}}
		setFormData={false}
	/>)

	expect(getByText('Loading...')).toBeInTheDocument();
	expect(container.querySelector('input[name="firstName"]')).toBeDisabled();
	expect(container.querySelector('input[name="lastName"]')).toBeDisabled();
	expect(container.querySelector('input[name="email"]')).toBeDisabled();
	expect(container.querySelector('input[name="subject"]')).toBeDisabled();
	expect(container.querySelector('textarea[name="message"]')).toBeDisabled();
	expect(container.querySelector('button[name="submit"]')).toBeDisabled();

})

test('renders prefilled component', () => {
	const {getByDisplayValue} = render(<Form
		isLoading={false}
		isEditor={false}
		handleSubmit={() => {
		}}
		formData={{
			firstName: 'John', lastName: 'Doe', email: 'john@doe.com'
		}}
		setFormData={false}
	/>)

	expect(getByDisplayValue('John')).toBeInTheDocument();
	expect(getByDisplayValue('Doe')).toBeInTheDocument();
	expect(getByDisplayValue('john@doe.com')).toBeInTheDocument();
})

test('renders restricted component for editor', () => {
	const {container} = render(<Form
		isLoading={false}
		isEditor={true}
		handleSubmit={() => {
		}}
		formData={{}}
		setFormData={false}
	/>)

	expect(container.querySelector('input[name="firstName"]')).toBeDisabled();
	expect(container.querySelector('input[name="lastName"]')).toBeDisabled();
	expect(container.querySelector('input[name="email"]')).toBeDisabled();
	expect(container.querySelector('input[name="subject"]')).toBeDisabled();
	expect(container.querySelector('textarea[name="message"]')).toBeDisabled();
	expect(container.querySelector('button[name="submit"]')).toBeDisabled();
})

test('disallow submitting when any of the fields is empty', () => {
	const handleSubmit = jest.fn();
	const {container}  = render(<Form
		isLoading={false}
		isEditor={false}
		handleSubmit={handleSubmit}
		formData={{
			firstName: 'John', lastName: 'Doe', email: 'john@doe.com'
		}}
		setFormData={false}
	/>)

	const submitButton = container.querySelector('button[name="submit"]');

	fireEvent.click(submitButton);

	expect(handleSubmit).toHaveBeenCalledTimes(0);
})

test('submit when all fields are provided', () => {
	const handleSubmit           = jest.fn();
	const {container, getByText} = render(<Form
		isLoading={false}
		isEditor={false}
		handleSubmit={handleSubmit}
		formData={{
			firstName: 'John',
			lastName: 'Doe',
			email: 'john@doe.com',
			subject: 'Hello world!',
			message: 'Hello, world! This needs to have 30 chars or more.'
		}}
		setFormData={() => {
		}}
	/>)

	const form = container.querySelector('form');

	fireEvent.submit(form);

	expect(handleSubmit).toHaveBeenCalledTimes(1);

	waitFor(() => {
		expect(getByText('Loading...')).toBeInTheDocument();
	}, {timeout: 500})

	waitFor(() => {
		expect(getByText('Thank you for sending us your feedback')).toBeInTheDocument();
	}, {timeout: 1000})
})

test('displays loader and feedback component after submit', () => {
	const handleSubmit           = jest.fn();
	const {container, getByText} = render(<Form
		isLoading={false}
		isEditor={false}
		handleSubmit={handleSubmit}
		formData={{
			firstName: 'John',
			lastName: 'Doe',
			email: 'john@doe.com',
			subject: 'Hello world!',
			message: 'Hello, world! This needs to have 30 chars or more.'
		}}
		setFormData={() => {
		}}
	/>)

	const form = container.querySelector('form');

	fireEvent.submit(form);

	waitFor(() => {
		expect(getByText('Loading...')).toBeInTheDocument();
	}, {timeout: 500})

	waitFor(() => {
		expect(getByText('Thank you for sending us your feedback')).toBeInTheDocument();
	}, {timeout: 1000})
})

