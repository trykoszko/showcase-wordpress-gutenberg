import React from 'react';
import {__} from "@wordpress/i18n";

export const Loader = () => (
	<div className="mt-form-block-element__loader">
		<div className="mt-form-block-element__loader__spinner">
			<p className="mt-form-block-element__loader__spinner__text">{__('Loading...', 'mt-form')}</p>
		</div>
	</div>
)
