import {render, Suspense} from '@wordpress/element';
import domReady from '@wordpress/dom-ready';
import {Loader} from "./components/Loader";
import {TableWrapper} from "./components/TableWrapper";

domReady(() => {
	const forms = document.querySelectorAll('.wp-block-mt-form-mt-form-entries-table-wrapper, .wp-block-mt-form-mt-form-entries-table');
	if (forms?.length) {
		Array.from(forms).forEach(form => {
			render(
				<Suspense fallback={<Loader/>}>
					<TableWrapper blockData={blockData}/>
				</Suspense>,
				form
			)
		});
	}
});
