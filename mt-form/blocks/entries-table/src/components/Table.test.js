import React from 'react';
import {fireEvent, render, waitFor} from '@testing-library/react'
import '@testing-library/jest-dom'
import {Table} from "./Table";

test('renders table component', () => {
	const {container, getByText} = render(<Table
		isLoading={false}
		data={{}}
		setData={() => {
		}}
		fetchEntries={() => {
		}}
		page={null}
		setPage={(page = 1) => {
		}}
	/>)

	expect(container.querySelector('.mt-form-entries-table-component')).toBeInTheDocument();
	expect(getByText('Fetch entries')).toBeInTheDocument();
})

test('renders form component with loading overlay', () => {
	const {getByText, container} = render(<Table
		isLoading={true}
		data={{}}
		setData={() => {
		}}
		fetchEntries={() => {
		}}
		page={null}
		setPage={(page = 1) => {
		}}
	/>)

	expect(getByText('Loading...')).toBeInTheDocument();
})

test('allows clicking on fetch and displays loader and table after', () => {
	const handleSubmit           = jest.fn();
	const {getByText, container} = render(<Table
		isLoading={false}
		data={{}}
		setData={() => {
		}}
		fetchEntries={handleSubmit}
		page={null}
		setPage={(page = 1) => {
		}}
	/>)

	const button = getByText('Fetch entries');

	fireEvent.click(button);

	expect(handleSubmit).toHaveBeenCalledTimes(1);

	waitFor(() => {
		expect(getByText('Loading...')).toBeInTheDocument();
	}, {timeout: 500})

	waitFor(() => {
		expect(container.querySelector('.mt-form-entries-table-component__table')).toBeInTheDocument();
	}, {timeout: 1000})
})

test('paginates after fetching entries', () => {
	const handleSubmit           = jest.fn();
	const {getByText, container} = render(<Table
		isLoading={false}
		data={{}}
		setData={() => {
		}}
		fetchEntries={handleSubmit}
		page={null}
		setPage={(page = 1) => {
		}}
	/>)

	const button = getByText('Fetch entries');
	fireEvent.click(button);

	waitFor(() => {
		expect(container.querySelector('.mt-form-entries-table-component__table')).toBeInTheDocument();
	}, {timeout: 1000})

	waitFor(() => {
		const nextButton = getByText('Next page »');
		fireEvent.click(nextButton);

		expect(handleSubmit).toHaveBeenCalledTimes(2);
	})

	waitFor(() => {
		const prevButton = getByText('Previous page');
		fireEvent.click(prevButton);

		expect(handleSubmit).toHaveBeenCalledTimes(3);
	}, {timeout: 1000})
})
