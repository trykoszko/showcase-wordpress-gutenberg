import React from 'react';
import {__} from "@wordpress/i18n";
import {Loader} from "./Loader";

export const Table = (props) => {
	const {
			  isLoading    = false,
			  data         = {},
			  setData      = () => {
			  },
			  fetchEntries = () => {
			  },
			  page         = null,
			  setPage      = (page = 1) => {
			  }
		  } = props;

	return (
		<div className="mt-form-entries-table-component">

			<h3 className={'mt-form-entries-table-component__title'}>{__('Form submissions', 'mt-form')}</h3>

			{isLoading ? (
				<div className="mt-form-entries-table-component__loading-overlay">
					<Loader/>
				</div>
			) : (
				(data?.data?.length > 0) ? (
					<div className={'mt-form-entries-table-component__table'}>
						<div
							className={"mt-form-entries-table-component__table__row mt-form-entries-table-component__table__row--heading"}>
							<p>{__('First name', 'mt-form')}</p>
							<p>{__('Last name', 'mt-form')}</p>
							<p>{__('Email', 'mt-form')}</p>
							<p>{__('Subject', 'mt-form')}</p>
						</div>
						{data?.data?.map((entry, index) => (
							<div className={"mt-form-entries-table-component__table__row"} key={index}
								 onClick={() => {
									 setData({
										 ...data,
										 data: data?.data?.map((item, i) => {
											 if (i === index) {
												 return {
													 ...item,
													 isActive: !item.isActive
												 }
											 }
											 return item;
										 })
									 })
								 }}>
								<p>{entry.first_name}</p>
								<p>{entry.last_name}</p>
								<p>{entry.email}</p>
								<p>{entry.subject}</p>
								<p
									className={`mt-form-entries-table-component__table__row__message ${entry?.isActive ? 'mt-form-entries-table-component__table__row__message--active' : ''}`}
								>
									{entry.message}
								</p>
							</div>
						))}
						<div className={'mt-form-entries-table-component__pagination'}>
							{(data?.meta?.page > 1) ? (
								<button onClick={() => {
									setPage(page - 1)
								}}>&laquo; {__('Previous page', 'mt-form')}</button>
							) : (<></>)}
							{(data?.meta?.total > (data?.meta?.limit * data?.meta?.page)) ? (
								<button onClick={() => {
									console.log('a', page + 1);
									setPage(page + 1)
								}}>{__('Next page', 'mt-form')} &raquo;</button>
							) : (<></>)}
						</div>
					</div>
				) : (
					<button className={'mt-form-entries-table-component__fetch-button'}
							onClick={() => fetchEntries()}
					>
						{__('Fetch entries', 'mt-form')}
					</button>
				)
			)}
		</div>
	);

};
