import React from 'react';
import {render} from '@testing-library/react'
import '@testing-library/jest-dom'
import {Loader} from "./Loader";

test('renders loader component', () => {
	const {getByText} = render(<Loader/>)

	expect(getByText('Loading...')).toBeInTheDocument();
})
