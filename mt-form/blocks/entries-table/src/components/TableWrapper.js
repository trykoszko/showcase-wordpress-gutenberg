import React, {useEffect, useRef, useState} from 'react';
import {Table} from "./Table";

export const TableWrapper = (props) => {
	const {blockData}               = props;
	const [isLoading, setIsLoading] = useState(false);
	const [error, setError]         = useState(null);
	const [page, setPage]           = useState(1);
	const [data, setData]           = useState(null);
	const isMounted                 = useRef(false);

	useEffect(() => {
		const performFetch = async () => {
			if (isMounted.current) {
				await fetchEntries();
			} else {
				isMounted.current = true;
			}
		};

		performFetch();
	}, [page]);

	const fetchEntries = async () => {
		setError(null);
		setIsLoading(true);

		const requestUrlParams = new URLSearchParams()
		requestUrlParams.append('action', 'get_mt_form_submissions')
		requestUrlParams.append('nonce', blockData.nonce)
		requestUrlParams.append('page', +page)

		const response     = await fetch(`${blockData.ajaxUrl}?${requestUrlParams}`);
		const responseBody = await response.json();

		if (response.ok) {
			setData(responseBody ?? []);
		} else {
			setError(responseBody?.message ?? __('An error occurred', 'mt-form'));
		}

		setIsLoading(false);
	}

	return (
		<div className={"wp-block-mt-form-mt-form-entries-table-wrapper"}>
			{error ? (
				<p className={"mt-form-entries-table-component-error"}>{error}</p>
			) : (
				<Table
					isLoading={isLoading}
					data={data}
					setData={setData}
					fetchEntries={fetchEntries}
					page={page}
					setPage={setPage}
				/>
			)}
		</div>
	)
}
