import React from 'react';
import {render} from '@testing-library/react'
import '@testing-library/jest-dom'
import {TableWrapper} from "./TableWrapper";

test('renders table-wrapper component with table component inside', () => {
	const {container} = render(<TableWrapper
		blockData={{
			ajaxUrl: 'http://localhost/wp-admin/admin-ajax.php',
			nonce: 'test-nonce-123'
		}}
	/>)

	expect(container.querySelector('.wp-block-mt-form-mt-form-entries-table-wrapper')).toBeInTheDocument();
	expect(container.querySelector('.mt-form-entries-table-component')).toBeInTheDocument();
})
