<?php

/**
 * Plugin version
 */
const MT_FORM_VERSION       = '0.1.0';

/**
 * Plugin database table name
 */
const MT_FORM_DB_TABLE_NAME = 'mt_form_submissions';
