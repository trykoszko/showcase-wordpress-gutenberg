# MT Form Block

A WordPress plugin that adds two custom blocks to the Gutenberg editor:

- **MT Form**: A block that allows users to send a submission via a convenient contact form.
- **MT Form Table**: A block that displays the results of the form submissions and is only visible for Administrators.

## Features

- Two custom blocks for the Gutenberg editor
- Custom database table for storing form submissions
- Form validation and sanitization

## Installation

1. Download the plugin .zip file
2. Go to the WordPress admin panel and click on Plugins > Add New
3. Click on the "Upload Plugin" button and select the .zip file
4. Activate the plugin
5. Use the blocks in the Gutenberg editor
6. To view the form submissions, add the "MT Form Table" block to a page or post

## Testing

There are two levels of testing:

1. **PHP Unit tests**: Run the following command in /src directory to run the unit tests:

```bash
$ composer install
$ vendor/bin/phpunit
```

2. **JavaScript Unit tests**: Run the following command in blocks directories to run the JavaScript tests:

```bash
$ cd blocks/form
$ npm install
$ npm run test:unit
```

```bash
$ cd blocks/entries-table
$ npm install
$ npm run test:unit
```

### Author: Michal Trykoszko
